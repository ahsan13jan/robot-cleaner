﻿using System.Collections.Generic;

namespace Model
{
    public class Office
    {
        readonly static int MAX_X = 100000;
        readonly static int MIN_X = -100000;
        readonly static int MAX_Y = 100000;
        readonly static int MIN_Y = -100000;

        private readonly List<string> _cleanedTiles;

        public int NumberOfTilesCleaned { get; set; }

        public Office()
        {
            _cleanedTiles = new List<string>();
            NumberOfTilesCleaned = 0;
        }

        public static bool IsValid(Coordinate position )
        {
            if (position.X >= MIN_X && position.Y >= MIN_Y && position.X <= MAX_X && position.Y <= MAX_Y)
                return true;
            else
                return false;
        }

        public bool CleanTile(Coordinate newPosition)
        {
            bool cleaned = false;

            if ( IsValid(newPosition) )
            {
                if (!_cleanedTiles.Contains(newPosition.ToString()))
                {
                    _cleanedTiles.Add(newPosition.ToString());
                    NumberOfTilesCleaned++;
                    cleaned = true;
                }
            }
            return cleaned;
        }
    }
}
