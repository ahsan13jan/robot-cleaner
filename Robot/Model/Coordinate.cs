﻿using System;

namespace Model
{
    public class Coordinate
    {
        public int X { set; get; }

        public int Y { set; get; }

        public Coordinate( int x , int y)
        {
            this.X = x;
            this.Y = y;
        }

        public override string ToString()
        {
            return string.Format($"{X},{Y}");
        }

        public static Coordinate Parse (string str)
        {
            var splitStr = str.Split(' ');
            var x = Convert.ToInt32(splitStr[0]);
            var y = Convert.ToInt32(splitStr[1]);
            return new Coordinate(x, y);
        }

        public static bool operator == (Coordinate coordinate1, Coordinate coordinate2)
        {
            return coordinate1.Equals(coordinate2);
        }

        public static bool operator != (Coordinate coordinate1, Coordinate coordinate2)
        {
            return !coordinate1.Equals(coordinate2);
        }

        private bool Equals ( Coordinate coordinate )
        {
            return X == coordinate.X && Y == coordinate.Y;
        }
    }
}
