﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Model
{
    public class Input
    {
        public int numberOfCommands = 0;

        public List<string> commands = new List<string>();

        public Coordinate start;

        private bool Equals(Input input)
        {
            if (numberOfCommands != input.numberOfCommands)
                return false;

            if (start != input.start)
                return false;

            if (commands.Count() != input.commands.Count())
                return false;

            if (!commands.All(cmd => input.commands.Contains(cmd)))
                return false;

            return true;
        }

        public static Direction ParseDirection(string str)
        {
            switch (str)
            {
                case "N":
                    return Direction.North;
                case "S":
                    return Direction.South;
                case "E":
                    return Direction.East;
                case "W":
                    return Direction.West;
                default:
                    return Direction.North;
            }
        }

        public static Input Parse(string str)
        {
            var split = str.Split('|');
            var input = new Input
            {
                numberOfCommands = Convert.ToInt32(split[0]),
                start = Coordinate.Parse(split[1]),
                commands = new List<string>()
            };

            for (int i=2; i< split.Length; i++ )
            {
                input.commands.Add(split[i]);
            }

            return input;
        }

        public static Input TakeInputs()
        {
            string inputStr = string.Empty;
            Console.WriteLine("Enter the number of commands");
            var numberOfCommands = Console.ReadLine();

            Console.WriteLine("Enter the starting position");
            var startingPosition = Console.ReadLine();

            var commands = new List<string>();
            for (int i = 0; i < Convert.ToInt32(numberOfCommands) ; i++)
            {
                Console.WriteLine($"Enter {i + 1} command - (Direction Distance)");
                commands.Add(Console.ReadLine());
            }

            return Parse($"{numberOfCommands}|{startingPosition}|{commands.Aggregate((a, b) => a + "|" + b)}");
        }

        public static bool operator ==(Input input1 , Input input2)
        {
            return input1.Equals(input2);
        }

        public static bool operator !=(Input input1, Input input2)
        {
            return !input1.Equals(input2);
        }
    }
}