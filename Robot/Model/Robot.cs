﻿using System;
using System.Collections.Generic;

namespace Model
{
    public class Robot
    {
        private Coordinate _position;
        private Coordinate Position
        {
            set
            {
                if (Office.CleanTile(value))
                {
                    _position = value;
                }
            }
            get
            {
                return _position;
            }
        }

        public int NumberOfTilesCleaned
        {
            get
            {
                return Office.NumberOfTilesCleaned;
            }
        }

        private readonly Office Office;

        public Robot(Coordinate start)
        {
            Office = new Office();
            Position = start;
        }

        public void CleanFloor ( Direction Direction , int Distance )
        {
            if (Distance < 0)
            {
                Distance = 0;
            }

            for (int i=0; i< Distance; i++)
            {
                switch (Direction)
                {
                    case Direction.East:
                        Position = new Coordinate(Position.X + 1, Position.Y);
                        break;
                    case Direction.West:
                        Position = new Coordinate(Position.X - 1, Position.Y);
                        break;
                    case Direction.North:
                        Position = new Coordinate(Position.X, Position.Y + 1);
                        break;
                    case Direction.South:
                        Position = new Coordinate(Position.X, Position.Y - 1);
                        break;
                }
            }
        }

        public int ConsumeInput( List<string> commands )
        {
            commands.ForEach(cmd => CleanFloor( Direction: Input.ParseDirection(cmd.Split(' ')[0]), Distance: Convert.ToInt32(cmd.Split(' ')[1])));
            return Office.NumberOfTilesCleaned;
        }

        public void PrintCleanedBlocks()
        {
            Console.WriteLine($"Cleaned blocks = {Office.NumberOfTilesCleaned}");
        }
    }
}
