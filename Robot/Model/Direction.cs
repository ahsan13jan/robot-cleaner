﻿namespace Model
{
    public enum Direction
    {
        East,
        West,
        South,
        North
    }
}