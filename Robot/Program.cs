﻿using Model;
using System;
using System.Collections.Generic;

namespace Space
{
    class Program
    {
        static void Main(string[] args)
        {
            var input =  Input.TakeInputs();

            var robot = new Robot(input.start);

            robot.ConsumeInput(input.commands);

            robot.PrintCleanedBlocks();

        }
    }
}
