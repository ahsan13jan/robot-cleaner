using Model;
using Xunit;

namespace Test
{
    public class OfficeTests
    {
        [Theory]
        [InlineData(100, 100 , true)]
        [InlineData(100001, 100 , false)]
        [InlineData(100001, -100001, false)]
        [InlineData(100000, -100000, true)]
        public void Office_Check_Validity_Coordinates(int x , int y , bool expected)
        {
            var position = new Coordinate(x, y);

            var result = Office.IsValid(position);

            Assert.True(result == expected);
        }

        [Fact]
        public void Office_Clean_Unique_Tiles()
        {
            var position = new Coordinate(100, 100);
            var office = new Office();

            var status1 = office.CleanTile(position);
            position.X++;
            var status2 = office.CleanTile(position);

            Assert.True(status1 && status2 && office.NumberOfTilesCleaned == 2);
        }

        [Fact]
        public void Office_Clean_Same_Tile()
        {
            var position = new Coordinate(100, 100);
            var office = new Office();

            var status1 = office.CleanTile(position);
            var status2 = office.CleanTile(position);

            Assert.True(status1 && !status2 && office.NumberOfTilesCleaned == 1);
        }
    }
}
