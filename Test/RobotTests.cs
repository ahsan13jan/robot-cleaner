using Model;
using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class RobotTests
    {
        [Fact]
        public void Robot_CleanOutOfRange()
        {
            var robot = CreateRobotWithCoordinate(100000, 100000);

            robot.CleanFloor(Direction.East, 1);

            Assert.Equal(1, robot.NumberOfTilesCleaned);
        }

        [Fact]
        public void Robot_CleanNegativeRange()
        {
            var robot = CreateRobotWithCoordinate(0,0);

            robot.CleanFloor(Direction.East, -1);

            Assert.Equal(1, robot.NumberOfTilesCleaned);
        }

        [Fact]
        public void Robot_CleanInRange()
        {
            var robot = CreateRobotWithCoordinate(0, 0);

            robot.CleanFloor(Direction.East, 10000);

            Assert.True(true);
        }

        [Fact]
        public void Robot_CleanUniquePlaces()
        {
            var robot = CreateRobotWithCoordinate(100,200);
            var commands = new List<string>() { "E 2", "N 1" };

            var result = robot.ConsumeInput(commands);
            Assert.Equal(4, result);
        }

        [Fact]
        public void Robot_CleanDuplicatePlaces()
        {
            var robot = CreateRobotWithCoordinate(100,200);
            var commands = new List<string>() { "E 2", "N 1", "S 1" };

            var result = robot.ConsumeInput(commands);
            Assert.Equal(4, result);
        }

        private Robot CreateRobotWithCoordinate(int x , int y)
        {
            return new Robot(new Coordinate(x, y));
        }
    }
}
