using Model;
using System;
using System.Collections.Generic;
using Xunit;

namespace Test
{
    public class InputTests
    {
        [Fact]
        public void ParseInputText()
        {
            var inputText = @"2|10 42|E 2|N 1";

            var result = Input.Parse(inputText);

            var targetResult = new Input() {
                numberOfCommands = 2, 
                start = new Coordinate(10, 42), 
                commands = new List<string>() { "E 2", "N 1" } 
            };

            Assert.True(result == targetResult);
        }

        [Theory]
        [InlineData("N", Direction.North)]
        [InlineData("S", Direction.South)]
        [InlineData("E", Direction.East)]
        [InlineData("W", Direction.West)]
        public void Direction_Parse( string input , Direction expected )
        {
            var result = Input.ParseDirection(input);

            Assert.True( result.Equals(expected));
        }
    }
}
