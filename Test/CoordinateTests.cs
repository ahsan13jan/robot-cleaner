using Model;
using Xunit;

namespace Test
{
    public class CoordinateTests
    {
        [Theory]
        [InlineData(100, 100)]
        [InlineData(-11, 100)]
        public void Coordinate_Create(int x , int y)
        {
            var position = new Coordinate(x, y);

            Assert.True( position.X == x && position.Y == y);
        }

        [Theory]
        [InlineData(100, 100, "100,100")]
        [InlineData(-11, 100, "-11,100")]
        [InlineData(100000, 100000, "100000,100000")]
        [InlineData(-100000, -100000, "-100000,-100000")]
        public void Coordinate_ToString(int x , int y , string expected)
        {
            var position = new Coordinate(x, y);

            var result = position.ToString();
            Assert.True(result.Equals(expected));
        }
    }
}
